# Simple Hugo Theme

This is a simple [Hugo](https://gohugo.io) theme created for my personal needs.
It tries to output simple HTML that can be easly styled and rendered in minimal web browsers, the default style is [simple.css](https://simplecss.org).

## Installing
- ```git submodule add https://codeberg.org/didek/simple-hugo-theme themes/simple-hugo-theme```
- ```echo "theme = 'simple-hugo-theme'" >> config.toml```

## Additional params

### Global (in config.yaml)
params/slogan
: Slogan displayed under page title

params/licence
: Licence of the site, displayed in footer

params/author
: Author of the site

## Licence
This theme is licenced under Expat (MIT) licence.
